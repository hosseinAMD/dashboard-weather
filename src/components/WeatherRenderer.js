import React from "react";
import PropTypes from "prop-types";
import WeatherUI from "./WeatherUI";

const WeatherRenderer = (props) => {
  const { cityObject, degree, type, loading } = props;
  if (!loading) {
    if (cityObject) {
      return (
        <div className="col-md-6 col-sm-12">
          <div
            className="col-md-6 col-sm-6 text-right"
            style={{ paddingLeft: "10px" }}
          >
            <h3 style={{ color: "#DDDDDE" }}>
              <span className="fa  fa-map-marker"></span> {cityObject.name}
            </h3>
            <h1 style={{ marginTop: "-10px", color: "#ddd" }}>
              {degree}
              <sup>o</sup>
            </h1>
          </div>
          <div className="col-md-6 col-sm-6">
            <WeatherUI type={type} />
          </div>
        </div>
      );
    } else {
      return (
        <div className="col-md-6 col-sm-12">
          <div
            className="col-md-6 col-sm-6 text-right"
            style={{ paddingLeft: "10px" }}
          >
            <h3 style={{ color: "#DDDDDE" }}>
              <span className="fa  fa-map-marker"></span> Select City
            </h3>
            <h1 style={{ marginTop: "-10px", color: "#ddd" }}></h1>
          </div>
        </div>
      );
    }
  } else {
    return (
      <div className="col-md-6 col-sm-12">
        <div
          className="col-md-6 col-sm-6 text-right"
          style={{ paddingLeft: "10px" }}
        >
          <h3 style={{ color: "#DDDDDE" }}>
            <span className="fa  fa-map-marker"></span> {cityObject.name}
          </h3>
          <h1 style={{ marginTop: "-10px", color: "#ddd" }}></h1>
        </div>
        <div className="col-md-6 col-sm-6">
          <img
            src="/asset/loading.svg"
            alt="loading"
            style={{ width: "150px" }}
          />
        </div>
      </div>
    );
  }
};

WeatherRenderer.propTypes = {
  cityObject: PropTypes.object,
  degree: PropTypes.number,
  loading: PropTypes.bool,
  type: PropTypes.number,
};

export default WeatherRenderer;
