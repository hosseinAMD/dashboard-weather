import React from "react";

const Header = () => (
  <nav className="navbar navbar-default header navbar-fixed-top">
    <div className="col-md-12 nav-wrapper">
      <div className="navbar-header" style={{ width: "100%" }}>
        <div className="opener-left-menu is-open">
          <span className="top"></span>
          <span className="middle"></span>
          <span className="bottom"></span>
        </div>
        <a href="index.html" className="navbar-brand">
          <b>SADAD</b>
        </a>

        <ul className="nav navbar-nav search-nav">
          <li>
            <div className="search">
              <span
                className="fa fa-search icon-search"
                style={{ fontSize: 23 }}
              ></span>
              <div className="form-group form-animate-text">
                <input type="text" className="form-text" required />
                <span className="bar"></span>
                <label className="label-search">
                  Type anywhere to <b>Search</b>{" "}
                </label>
              </div>
            </div>
          </li>
        </ul>

        <ul className="nav navbar-nav navbar-right user-nav">
          <li className="user-name">
            <span>Hossein Ahmadi</span>
          </li>
          <li className="dropdown avatar-dropdown">
            <img
              src="asset/img/avatar.jpg"
              className="img-circle avatar"
              alt="user name"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="true"
            />
            <ul className="dropdown-menu user-dropdown">
              <li>
                <a href="#">
                  <span className="fa fa-user"></span> My Profile
                </a>
              </li>
              <li>
                <a href="#">
                  <span className="fa fa-calendar"></span> My Calendar
                </a>
              </li>
              <li role="separator" className="divider"></li>
              <li className="more">
                <ul>
                  <li>
                    <a href="">
                      <span className="fa fa-cogs"></span>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <span className="fa fa-lock"></span>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <span className="fa fa-power-off "></span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li>
            <a href="#" className="opener-right-menu">
              <span className="fa fa-coffee"></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
);

export default Header;
