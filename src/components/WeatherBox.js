import React, { useState } from "react";
import { defaultCities } from "../data/defaultCities";
import WeatherRenderer from "./WeatherRenderer";
import { weatherToken } from "../data/API_KEY";
import CitySuggestionBox from "./CitySuggestionBox";

const WeatherBox = (props) => {
  const [cityID, setCityID] = useState("");
  const [cityObject, setCityObject] = useState(undefined);
  const [loading, setLoading] = useState(false);
  const [weather, setWeather] = useState({
    degree: 0,
    type: 0,
  });

  const handleSelectCity = (id) => {
    const cityObject = defaultCities.find((city) => city.id === id);
    if (cityObject) {
      setCityObject(cityObject);
      setCityID(id);
      handleGetWeatherStatus(cityObject.api_value);
    } else {
      setCityObject("");
      setCityID("");
    }
  };

  const handleGetWeatherStatus = (name) => {
    setLoading(true);
    fetch(
      `http://api.openweathermap.org/data/2.5/weather?q=${name}&appid=${weatherToken}`
    )
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.cod === 200) {
            const degree = parseInt(result.main.temp - 273.15);
            const type = result.weather[0].id;
            setWeather({
              degree,
              type,
            });
            setLoading(false);
          } else {
            alert(result.message);
          }
        },
        (res) => {
          alert(res.response.message);
        }
      );
  };

  return (
    <div className="panel">
      <div className="panel-body" style={{ height: "230px" }}>
        <div className="col-md-6 col-sm-12">
          <h3 className="animated fadeInLeft">Customer Service</h3>
          <p className="animated fadeInDown">
            {cityObject ? (
              <>
                {" "}
                <span className="fa  fa-map-marker"></span> {cityObject.name},
                {cityObject.country}
              </>
            ) : (
              "Please select a city:"
            )}
          </p>

          <ul className="nav navbar-nav">
            {defaultCities.map((city) => (
              <li
                key={city.id}
                className={city.id === cityID ? "selected-city" : ""}
                onClick={() => handleSelectCity(city.id)}
                style={{ cursor: "pointer" }}
              >
                <a>{city.name}</a>
              </li>
            ))}
          </ul>
          <CitySuggestionBox getWeathers={props.getWeathers} tableLoading={props.tableLoading} />
        </div>
        <WeatherRenderer
          loading={loading}
          cityObject={cityObject}
          degree={weather.degree}
          type={weather.type}
        />
        <div className="col-md-6 col-sm-12"></div>
      </div>
    </div>
  );
};

export default WeatherBox;
