import React from "react";

const Sidebar = () => (
  <div id="left-menu">
    <div className="sub-left-menu scroll">
      <ul className="nav nav-list">
        <li>
          <div className="left-bg"></div>
        </li>
        <li className="time">
          <h1 className="animated fadeInLeft">21:00</h1>
          <p className="animated fadeInRight">Sat,October 1st 2029</p>
        </li>
        <li className="active ripple">
          <a className="tree-toggle nav-header">
            <span className="fa-home fa"></span> Dashboard
            <span className="fa-angle-right fa right-arrow text-right"></span>
          </a>
          <ul className="nav nav-list tree">
            <li>
              <a href="dashboard-v1.html">Dashboard v.1</a>
            </li>
            <li>
              <a href="dashboard-v2.html">Dashboard v.2</a>
            </li>
          </ul>
        </li>
        <li className="ripple">
          <a className="tree-toggle nav-header">
            <span className="fa-diamond fa"></span> Layout
            <span className="fa-angle-right fa right-arrow text-right"></span>
          </a>
          <ul className="nav nav-list tree">
            <li>
              <a href="topnav.html">Top Navigation</a>
            </li>
            <li>
              <a href="boxed.html">Boxed</a>
            </li>
          </ul>
        </li>
        <li className="ripple">
          <a className="tree-toggle nav-header">
            <span className="fa-area-chart fa"></span> Charts
            <span className="fa-angle-right fa right-arrow text-right"></span>
          </a>
          <ul className="nav nav-list tree">
            <li>
              <a href="chartjs.html">ChartJs</a>
            </li>
            <li>
              <a href="morris.html">Morris</a>
            </li>
            <li>
              <a href="flot.html">Flot</a>
            </li>
            <li>
              <a href="sparkline.html">SparkLine</a>
            </li>
          </ul>
        </li>
        <li className="ripple">
          <a className="tree-toggle nav-header">
            <span className="fa fa-pencil-square"></span> Ui Elements{" "}
            <span className="fa-angle-right fa right-arrow text-right"></span>{" "}
          </a>
          <ul className="nav nav-list tree">
            <li>
              <a href="color.html">Color</a>
            </li>
            <li>
              <a href="weather.html">Weather</a>
            </li>
            <li>
              <a href="typography.html">Typography</a>
            </li>
            <li>
              <a href="icons.html">Icons</a>
            </li>
            <li>
              <a href="buttons.html">Buttons</a>
            </li>
            <li>
              <a href="media.html">Media</a>
            </li>
            <li>
              <a href="panels.html">Panels & Tabs</a>
            </li>
            <li>
              <a href="notifications.html">Notifications & Tooltip</a>
            </li>
            <li>
              <a href="badges.html">Badges & Label</a>
            </li>
            <li>
              <a href="progress.html">Progress</a>
            </li>
            <li>
              <a href="sliders.html">Sliders</a>
            </li>
            <li>
              <a href="timeline.html">Timeline</a>
            </li>
            <li>
              <a href="modal.html">Modals</a>
            </li>
          </ul>
        </li>
        <li className="ripple">
          <a className="tree-toggle nav-header">
            <span className="fa fa-check-square-o"></span> Forms{" "}
            <span className="fa-angle-right fa right-arrow text-right"></span>{" "}
          </a>
          <ul className="nav nav-list tree">
            <li>
              <a href="formelement.html">Form Element</a>
            </li>
            <li>
              <a href="#">Wizard</a>
            </li>
            <li>
              <a href="#">File Upload</a>
            </li>
            <li>
              <a href="#">Text Editor</a>
            </li>
          </ul>
        </li>
        <li className="ripple">
          <a className="tree-toggle nav-header">
            <span className="fa fa-table"></span> Tables{" "}
            <span className="fa-angle-right fa right-arrow text-right"></span>{" "}
          </a>
          <ul className="nav nav-list tree">
            <li>
              <a href="datatables.html">Data Tables</a>
            </li>
            <li>
              <a href="handsontable.html">handsontable</a>
            </li>
            <li>
              <a href="tablestatic.html">Static</a>
            </li>
          </ul>
        </li>
        <li className="ripple">
          <a href="calendar.html">
            <span className="fa fa-calendar-o"></span>Calendar
          </a>
        </li>
        <li className="ripple">
          <a className="tree-toggle nav-header">
            <span className="fa fa-envelope-o"></span> Mail{" "}
            <span className="fa-angle-right fa right-arrow text-right"></span>{" "}
          </a>
          <ul className="nav nav-list tree">
            <li>
              <a href="mail-box.html">Inbox</a>
            </li>
            <li>
              <a href="compose-mail.html">Compose Mail</a>
            </li>
            <li>
              <a href="view-mail.html">View Mail</a>
            </li>
          </ul>
        </li>
        <li className="ripple">
          <a className="tree-toggle nav-header">
            <span className="fa fa-file-code-o"></span> Pages{" "}
            <span className="fa-angle-right fa right-arrow text-right"></span>{" "}
          </a>
          <ul className="nav nav-list tree">
            <li>
              <a href="forgotpass.html">Forgot Password</a>
            </li>
            <li>
              <a href="login.html">SignIn</a>
            </li>
            <li>
              <a href="reg.html">SignUp</a>
            </li>
            <li>
              <a href="article-v1.html">Article v1</a>
            </li>
            <li>
              <a href="search-v1.html">Search Result v1</a>
            </li>
            <li>
              <a href="productgrid.html">Product Grid</a>
            </li>
            <li>
              <a href="profile-v1.html">Profile v1</a>
            </li>
            <li>
              <a href="invoice-v1.html">Invoice v1</a>
            </li>
          </ul>
        </li>
        <li className="ripple">
          <a className="tree-toggle nav-header">
            <span className="fa "></span> MultiLevel{" "}
            <span className="fa-angle-right fa right-arrow text-right"></span>{" "}
          </a>
          <ul className="nav nav-list tree">
            <li>
              <a href="view-mail.html">Level 1</a>
            </li>
            <li>
              <a href="view-mail.html">Level 1</a>
            </li>
            <li className="ripple">
              <a className="sub-tree-toggle nav-header">
                <span className="fa fa-envelope-o"></span> Level 1
                <span className="fa-angle-right fa right-arrow text-right"></span>
              </a>
              <ul className="nav nav-list sub-tree">
                <li>
                  <a href="mail-box.html">Level 2</a>
                </li>
                <li>
                  <a href="compose-mail.html">Level 2</a>
                </li>
                <li>
                  <a href="view-mail.html">Level 2</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="credits.html">Credits</a>
        </li>
      </ul>
    </div>
  </div>
);

export default Sidebar;
