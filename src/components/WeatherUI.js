import React from "react";
import PropTypes from "prop-types";

const WeatherUI = (props) => {
  const { type } = props;

  if (type > 800) {
    return (
      <div className="wheather">
        <div className="mostly-suny suny">
          <div className="sun animated pulse infinite"></div>
          <div className="cloudy animated pulse infinite">
            <div className="shadow"></div>
          </div>
        </div>

        <div className="hazy cloudy animated pulse infinite">
          <div className="shadow"></div>
        </div>
      </div>
    );
  } else if (type === 800) {
    return (
      <div className="suny">
        <div className="sun animated pulse infinite"></div>
        <div className="mount"></div>
        <div className="mount mount1"></div>
        <div className="mount mount2"></div>
      </div>
    );
  } else if (type >= 700 && type < 800) {
    return (
      <div className="tornado tornado-thunder">
        <div className="cloudy animated flash infinite">
          <div className="sub-wheather">
            <div className="thunder"></div>
          </div>
        </div>
        <div className="wind wind1"></div>
        <div className="wind wind2"></div>
        <div className="wind wind3"></div>
        <div className="wind wind4"></div>
      </div>
    );
  } else if (type >= 600 && type < 700) {
    return (
      <div>
        <div className="snowy rainy animated pulse infinite">
          <div className="shadow"></div>
        </div>
        <div className="sub-wheather snowy-sub-wheather">
          <div className="rain">
            <div className="droplet droplet1"></div>
            <div className="droplet droplet2"></div>
            <div className="droplet droplet3"></div>
            <div className="droplet droplet4"></div>
            <div className="droplet droplet5"></div>
            <div className="droplet droplet6"></div>
          </div>
        </div>
      </div>
    );
  } else if (type > 502 && type < 600) {
    return (
      <div className="wheather">
        <div className="stormy rainy animated pulse infinite">
          <div className="shadow"></div>
        </div>
        <div className="sub-wheather">
          <div className="thunder"></div>
          <div className="rain">
            <div className="droplet droplet1"></div>
            <div className="droplet droplet2"></div>
            <div className="droplet droplet3"></div>
            <div className="droplet droplet4"></div>
            <div className="droplet droplet5"></div>
            <div className="droplet droplet6"></div>
          </div>
        </div>
      </div>
    );
  } else if (type >= 500 && type < 503) {
    return (
      <div className="wheather">
        <div className="stormy rainy animated pulse infinite">
          <div className="shadow"></div>
        </div>
        <div className="sub-wheather">
          <div className="rain">
            <div className="droplet droplet1"></div>
            <div className="droplet droplet2"></div>
            <div className="droplet droplet3"></div>
            <div className="droplet droplet4"></div>
            <div className="droplet droplet5"></div>
            <div className="droplet droplet6"></div>
          </div>
        </div>
      </div>
    );
  } else if (type > 202 && type < 300) {
    return (
      <div className="wheather">
        <div className="stormy rainy animated pulse infinite">
          <div className="shadow"></div>
        </div>
        <div className="sub-wheather">
          <div className="thunder"></div>
          <div className="rain">
            <div className="droplet droplet1"></div>
            <div className="droplet droplet2"></div>
            <div className="droplet droplet3"></div>
            <div className="droplet droplet4"></div>
            <div className="droplet droplet5"></div>
            <div className="droplet droplet6"></div>
          </div>
          <div className="tornado">
            <div className="wind wind1"></div>
            <div className="wind wind2"></div>
            <div className="wind wind3"></div>
            <div className="wind wind4"></div>
          </div>
        </div>
      </div>
    );
  } else if (type >= 200 && type < 203) {
    return (
      <div className="tornado tornado-thunder">
        <div className="cloudy animated flash infinite">
          <div className="sub-wheather">
            <div className="thunder"></div>
          </div>
        </div>
        <div className="wind wind1"></div>
        <div className="wind wind2"></div>
        <div className="wind wind3"></div>
        <div className="wind wind4"></div>
      </div>
    );
  } else {
    return (
      <div className="suny">
        <div className="sun animated pulse infinite"></div>
        <div className="mount"></div>
        <div className="mount mount1"></div>
        <div className="mount mount2"></div>
      </div>
    );
  }
};

WeatherUI.propTypes = {
  type: PropTypes.number,
};

export default WeatherUI;
