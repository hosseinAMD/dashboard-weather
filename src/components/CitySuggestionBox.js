import React, { useEffect, useState } from "react";
import { suggestCities } from "../data/defaultCities";

const CitySuggestionBox = (props) => {
  useEffect(() => {
    fetch(`/asset/city.list.json`)
      .then((res) => res.json())
      .then(
        (result) => {
          console.log(result);
        },
        (error) => {
          console.log(error);
        }
      );
  }, []);

  return (
    <div className="form-group">
      <label htmlFor="city-select" style={{ marginRight: "10px" }}>
        Other Cities
      </label>
      <select
        className="select2-B"
        id="city-select"
        multiple="multiple"
        style={{ width: "300px" }}
      >
        {suggestCities.map((city) => (
          <option value={city.name} key={city.id}>
            {city.name}
          </option>
        ))}
      </select>
      <input
        type="button"
        className="btn btn-primary"
        onClick={props.getWeathers}
        value={props.tableLoading ? "Loading..." : "Get Weather"}
      />
    </div>
  );
};

export default CitySuggestionBox;
