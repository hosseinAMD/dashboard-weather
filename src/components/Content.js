import React, { useState } from "react";
import WeatherBox from "./WeatherBox";
import { weatherToken } from "../data/API_KEY";

const Content = () => {
  const [tableLoading, setTableLoading] = useState(false);
  const [weather, setWeather] = useState([]);

  const getWeathers = () => {
    const selectedCities = document.getElementById("city-select")
      .selectedOptions;
    const cityArray = [...selectedCities];
    setTableLoading(true);
    let weather = [];
    const weatherList = async () => {
      let i = 0;
      for (i; i < cityArray.length; i++) {
        const response = await fetch(
          `http://api.openweathermap.org/data/2.5/weather?q=${cityArray[i].value}&appid=${weatherToken}`
        );
        const result = await response.json();
        const degree = parseInt(result.main.temp - 273.15);
        const type = result.weather[0].main;
        weather.push({
          city: cityArray[i].value,
          degree,
          type,
        });
      }
    };
    weatherList().then(() => {
      setWeather(weather);
      setTableLoading(false);
    });
  };

  return (
    <div id="content">
      <WeatherBox getWeathers={getWeathers} tableLoading={tableLoading} />
      <div className="col-md-12" style={{ padding: "20px" }}>
        <div className="col-md-12 padding-0">
          <div className="col-md-12 padding-0">
            <div className="col-md-12 padding-0">
              <div className="col-md-12">
                <div className="panel box-v1">
                  <div className="panel-heading bg-white border-none">
                    <div className="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
                      <h4 className="text-left">Weather</h4>
                    </div>
                    <div className="col-md-6 col-sm-6 col-xs-6 text-right">
                      <h4>
                        <span className="fa fa-cloud icons icon text-right"></span>
                      </h4>
                    </div>
                  </div>
                  <div className="panel-body text-center">
                    <table
                      className="table table-striped table-bordered"
                      width="100%"
                    >
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>City</th>
                          <th>Temp</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        {weather.map((item, index) => (
                          <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{item.city}</td>
                            <td>{item.degree}</td>
                            <td>{item.type}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                    <hr />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Content;
