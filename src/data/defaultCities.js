export const defaultCities = [
  {
    id: 0,
    name: "Tehran",
    api_value: "tehran",
    country: "Islamic Republic of Iran",
  },
  {
    id: 1,
    name: "New York",
    api_value: "new york",
    country: "United States America",
  },
  {
    id: 2,
    name: "Paris",
    api_value: "paris",
    country: "France",
  },
  {
    id: 3,
    name: "Milan",
    api_value: "milan",
    country: "Italy",
  },
  {
    id: 4,
    name: "Moscow",
    api_value: "moscow",
    country: "Russia",
  },
  {
    id: 5,
    name: "Toronto",
    api_value: "toronto",
    country: "Canada",
  },
];

export const suggestCities = [
  {
    id: 0,
    name: "Mashhad",
    api_value: "mashhad",
    country: "Islamic Republic of Iran",
  },
  {
    id: 1,
    name: "Texas",
    api_value: "texas",
    country: "United States America",
  },
  {
    id: 2,
    name: "Lyon",
    api_value: "Lyon",
    country: "France",
  },
  {
    id: 3,
    name: "Madrid",
    api_value: "madrid",
    country: "Italy",
  },
  {
    id: 4,
    name: "Kazan",
    api_value: "kazan",
    country: "Russia",
  },
  {
    id: 5,
    name: "Montreal",
    api_value: "montreal",
    country: "Canada",
  },
];